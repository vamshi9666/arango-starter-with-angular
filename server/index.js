const express = require("express");
const loginRoutes = require("./login.routes")
const app = express();
const bodyParser =  require("body-parser")
const  cors = require('cors');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(cors())
app.use('/auth', loginRoutes)

app.listen(3000, () => {
    console.log("server started on port 3000");
    
})