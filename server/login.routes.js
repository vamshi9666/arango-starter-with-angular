const mockUser = {
    "name": "vamshi",
    "email": "vamshi9666@riseup.net",
    "password": "thiswillbehashed"
}


const router = require('express').Router()
const { db, userCollection } = require('./db')
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")

router.post("/login", (req, res, next) => {
    try {
        userCollection.all()
            .then(
                results => results.map(obj => {
                    if (obj.email === req.body.email) {
                        return obj
                    }

                })
            ).then(result => {
                console.log("result", result);
                if (result.length > 0) {
                    bcrypt.compare(req.body.password, result[0].password, (err, doc) => {

                        if (doc) {
                            const token = jwt.sign({
                                name: req.body.name
                            }, "jwtsecret", {
                                    expiresIn: "1h"
                                })
                            return res.status(200).json({
                                message: "user authenticated successfully !",
                                token: token
                            })
                        }
                        else {
                            res.json({
                                message:"wrong password",
                                err
                            })
                        }

                    })
                }
                else {
                    res.status(502).json({
                        message: "user not found",

                    })
                }


            })
    }
    catch (err) {
        res.status(500).json({
            message: "internal server error"
        })
    }
})

router.post("/signup", (req, res, next) => {
    try {
        userCollection.all()
            .then(
                results => results.map(obj => {
                    if (obj.email === req.body.email) {
                        return obj
                    }

                })
            ).then(results => { 
                results =  results.filter(a => a);
                
                if (results.length > 0) {
                    res.status(200).json({
                        status:"exists",
                        message: "user alreasy exists"
                    })
                }
                else {
                    bcrypt.hash(req.body.password, 10, (err, hash) => {
                        if (err) {
                            res.json({
                                status:"error",
                                message: " err",
                                err
                            })
                        }
                        else {
                            const newUser = {
                                "name": req.body.name,
                                "email": req.body.email,
                                "password": hash
                            };

                            userCollection.save(newUser).then(
                                meta => {
                                    console.log('Document saved:', meta._rev);
                                    res.status(201).json({
                                        status:"ok",
                                        message: "user created"
                                    })
                                },
                                err => {
                                    res.status(505).json({
                                        status :"error",
                                        
                                        err: err
                                    })
                                }
                            )
                        }
                    })
                }
            })


    }
    catch (err) {
        console.error("error in login.routes.js", err)
    }
})

module.exports = router