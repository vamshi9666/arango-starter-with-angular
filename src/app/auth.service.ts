import { Injectable } from '@angular/core';
import { HttpClient }    from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private login_url = 'http://localhost:3000/auth/login';
  private register_url = 'http://localhost:3000/auth/signup'
  constructor(private http : HttpClient) { 

  }
   registerNewUser(user){
    return   this.http.post<any>(this.register_url, user)
   }
   login (user) {
    return this.http.post<any>(this.login_url,user)
   }


}
