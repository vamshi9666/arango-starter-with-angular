import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({

    email: new FormControl(""),
    password: new FormControl("")
  })
  constructor(private auth: AuthService) {
  }

  ngOnInit() {
  }

  login() {
    this.auth.login(this.loginForm.value)
      .subscribe(res => {
        const { token, message } = res;
        if (token) {
          alert("user logged in successfully !  token was logged to console. this token can be used for further requests")
          console.log("token", token)
        }
        else {
          if (message === "wrong password"){
            alert("wrong password")
          }
          console.log(message)
        }

      },
        err => {
          console.error("err", err);

        })
  }

}
