import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  signUpForm = new FormGroup({
    name : new FormControl(""),
    email: new FormControl(""),
    password: new FormControl("")
  })
  constructor(private auth: AuthService, private router : Router) {
  }
  ngOnInit(){
    
  }
  signUp(){
    this.auth.registerNewUser(this.signUpForm.value)
      .subscribe(res => {
        const { status  } = res;
        if (status === "ok" ){
          alert("user created , please try to login" );
          
        }
        else if ( status === 'exists'){
          alert("user with given details alredy exist . please login");

        } 
        else {
          alert(" error in registering new user ")
        }
      },err => {
        console.error("err",err);
        
      })
  }

}
